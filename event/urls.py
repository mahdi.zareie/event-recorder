from __future__ import absolute_import
from django.conf.urls import url
from event import views

__author__ = 'Elfix'

urlpatterns = [
    url(r"^panel$", views.EventList.as_view(), name="panel"),
    url(r"^occurrence$", views.occurrence_information, name="occurrence_information"),

]
