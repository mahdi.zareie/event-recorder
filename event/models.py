from django.utils import timezone
from django.db import models
from django.contrib.auth.models import User


# Create your models here.


class Event(models.Model):
    user = models.ForeignKey(User)
    event_name = models.CharField(max_length=255)


class Occurrence(models.Model):
    event = models.ForeignKey(Event)
    value = models.DecimalField(decimal_places=2, max_digits=3, default=1)
    occurred_at = models.DateTimeField()

    def save(self, *args, **kwargs):
        self.occurred_at = timezone.now()
        super(Occurrence, self).save(*args, **kwargs)
