from __future__ import absolute_import
from celery import shared_task
from django.contrib.auth.models import User
from event.models import Event, Occurrence
from sms_ir.services import get_messages
from redis import Redis
from datetime import datetime

__author__ = 'Elfix'

SYSTEM_START_DATE = datetime.strptime("2016-03-20T00:00:00.0000", "%Y-%m-%dT%H:%M:%S.%f")


@shared_task
def load_occurrences():
    r = Redis()
    last_sync = r.get("LAST_SYNC")
    if last_sync is None:
        last_sync = SYSTEM_START_DATE
    else:
        last_sync = datetime.strptime(last_sync.decode('ascii'), "%Y-%m-%dT%H:%M:%S.%f")
    messages = get_messages(SYSTEM_START_DATE)
    print("New Messages count : %s" % len(messages))
    for message in messages:
        if message.LatinReceiveDateTime < last_sync:
            continue
        user = User.objects.filter(username=message.MobileNo).first()
        if user is None:
            print("User Not found : %s" % message.MobileNo)
            continue
        message_body_parts = message.SMSMessageBody.strip().split(":")
        if len(message_body_parts) != 2:
            print("Invalid Message : %s", message.SMSMessageBody)
            continue
        event = Event.objects.filter(id=message_body_parts[0]).first()
        if event is None:
            print("Event does not exists")
            continue
        if event.user_id != user.id:
            print("invalid event id")
            continue
        occurrence = Occurrence(event=event)
        occurrence.value = message_body_parts[1]
        occurrence.save()
        print("Occurrence Saved")
    last_sync = datetime.now()
    r.set("LAST_SYNC", last_sync.strftime("%Y-%m-%dT%H:%M:%S.%f"))
