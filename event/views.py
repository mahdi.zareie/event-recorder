from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import ListView
from django import http
from event.models import Occurrence, Event
import simplejson as json
from django.utils.timezone import timedelta
from django.utils import timezone as datetime


def occurrence_information(request):  # TODO: needs better query using aggregate functions
    if request.GET.get("event_id"):
        event = Event.objects.filter(id=request.GET.get("event_id")).first()
        if event is None:
            return http.HttpResponseNotFound()
        if event.user_id != request.user.id:
            return http.HttpResponseForbidden()
        labels = []
        values = []
        today = datetime.now() + timedelta(days=1)
        for i in range(int(request.GET.get("days", 10)), 0, -1):
            later_date = today - timedelta(days=i)
            former_date = today - timedelta(days=i + 1)
            labels.append(former_date.day)
            event_value = 0
            for occurrence in Occurrence.objects.filter(event=event,
                                                        occurred_at__lt=later_date,
                                                        occurred_at__gte=former_date).all():
                event_value += occurrence.value
            values.append(event_value)
        result = {
            'labels': labels,
            'values': values
        }
        return http.HttpResponse(content=json.dumps(result), content_type="application/json")


class EventList(ListView):
    template_name = "event/list.html"

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EventList, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        assert self.request.user.is_authenticated() is True
        from event.models import Event
        return Event.objects.filter(user_id=self.request.user.id).all()
