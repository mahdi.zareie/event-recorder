from __future__ import absolute_import

__author__ = 'Elfix'


def get_value_of_this_key(dictionary: dict, keys: str):
    keys = keys.split(" ")
    current = dictionary
    for index in range(0, len(keys)):
        if current is None:
            return None
        if keys[index] in current:
            current = current[keys[index]]
        else:
            return None

    return current
