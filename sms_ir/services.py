from __future__ import absolute_import
from requests.sessions import Session
import xmltodict
import datetime
from sms_ir.models import MessageObject
from sms_ir.utils import get_value_of_this_key

__author__ = 'Elfix'

WS_URL = "http://ip.sms.ir/ws/SendReceive.asmx"
USERNAME = "09350821730"
PASSWORD = "123123"


def get_messages(before: datetime) -> list:
    request_body = """<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <GetReceivedMessages xmlns="http://tempuri.org/">
      <userName>""" + USERNAME + """</userName>
      <password>""" + PASSWORD + """</password>
      <fromDate>""" + before.strftime("%Y-%m-%dT%H:%M:%S.%f+04:30") + """</fromDate>
      <toDate>""" + (datetime.datetime.now() + datetime.timedelta(days=3)).strftime("%Y-%m-%dT%H:%M:%S.%f+04:30") + """</toDate>
    </GetReceivedMessages>
  </soap:Body>
</soap:Envelope>"""
    url = WS_URL + "?op=GetReceivedMessages"
    soap_session = Session()
    soap_session.headers["Content-Type"] = "text/xml; charset=utf-8"
    response = soap_session.post(url, request_body)
    dict_value = get_value_of_this_key(
        xmltodict.parse(
            response.content.decode("utf8"),
            encoding="utf-8"),
        "soap:Envelope soap:Body GetReceivedMessagesResponse GetReceivedMessagesResult ReceivedMessage")
    if dict_value is None:
        print("No new Message")
        return []
    result = []
    for sms in dict_value:
        result.append(MessageObject(sms))
    return result


print(get_messages(datetime.datetime.now() - datetime.timedelta(days=10)))
