from django.apps import AppConfig


class SmsIrConfig(AppConfig):
    name = 'sms_ir'
