from datetime import datetime
from django.db import models


# Create your models here.

class MessageObject:
    def __init__(self, dict_data):
        self.ID = dict_data["ID"]
        self.LatinReceiveDateTime = datetime.strptime(dict_data["LatinReceiveDateTime"], "%Y-%m-%dT%H:%M:%S.%f")
        self.LineNumber = dict_data["LineNumber"]
        self.MobileNo = dict_data["MobileNo"]
        self.ReceiveDateTime = dict_data["ReceiveDateTime"]
        self.SMSMessageBody = dict_data["SMSMessageBody"]
        self.TypeOfMessage = dict_data["TypeOfMessage"]

    def __str__(self):
        return "{id} {mobile} {msg} {date}".format(id=self.ID, mobile=self.MobileNo, msg=self.SMSMessageBody,
                                                   date=self.LatinReceiveDateTime)
